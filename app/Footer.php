<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $table = "footer";
    protected $fillable = [
        'id', 'phone1', 'phone2', 'phone3', 'email1', 'email2', 'email3', 'facebook', 'instagram', 'twitter', 'open_and_close'
    ];
}
