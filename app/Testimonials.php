<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    protected $table = "testimonials";
    protected $fillable = [
        'id',
         'title',
         'description',
          'client_name',
          'imagePath'
    ];
}
