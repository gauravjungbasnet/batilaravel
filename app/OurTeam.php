<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OurTeam extends Model
{
    protected $table = "our_team";
    protected $fillable = [
        'id',
         'title',
         'description',
          'designation',
          'imagePath'
    ];
}
