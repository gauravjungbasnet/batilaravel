<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OurService extends Model
{
    protected $table = "our_service";
    protected $fillable = [
        'id',
         'title',
         'description',
          'author',
          'imagePath'
    ];
}
