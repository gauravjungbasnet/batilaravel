<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "product";
    protected $fillable = [
        'title',
         'category_id',
         'material',
         'core',
         'thickness',
         'popular_width',
         'popular_length',
         'color',
         'roll_size',
         'supply_capacity',
         'imagePath'
    ];
}
