<?php

namespace App\Http\Controllers\API;

use App\OurService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OurServiceController extends Controller
{
    public function store(Request $request){
        $ourservice=new OurService([
            'title'=>$request->get('title'),
            'description'=> $request->get('description'),
            'author'=> $request->get('author')
        ]);
        $ourservice->save();
        return response()->json($ourservice, 201);
    }

    public function index(){
        $ourservice=DB::select('select * from our_service');
        return response()->json($ourservice,200);
    }

    public function update(Request $request, $id){
        // $aboutUs  = update($request->all());
        $title = $request->input('title');

        $description = $request->input('description');
        $author = $request->input('author');
        $imagePath = $request->input('imagePath');
        $ourservice  = DB::update('update our_service set title = ?,description=?,author=?,imagePath=? where id = ?',[$title,$description,$author,$imagePath,$id]);
        return response()->json($ourservice,200);
    }
    public function destroy($id){
        $ourservice = OurService::where('id',$id)->delete();
        return response()->json(null, 204);
    }
}
