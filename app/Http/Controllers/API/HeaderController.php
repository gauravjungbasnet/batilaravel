<?php

namespace App\Http\Controllers\API;

use App\Header;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HeaderController extends Controller
{
    public function store(Request $request){
        $header=new Header([
            'name'=>$request->get('name'),
            'value'=> $request->get('value'),
            'status'=> $request->get('status'),
            'order'=> $request->get('order')
        ]);
        $header->save();
        return response()->json($header, 201);
    }

    public function index(){
        $header=DB::select('select * from header');
        return response()->json($header,200);

    }

    public function update(Request $request, Header $header){
        $header->update($request->all());
        return response()->json($header,200);
    }
    public function destroy(Request $request, Header $header){
        $header->delete();
        return response()->json(null, 204);
    }
}
