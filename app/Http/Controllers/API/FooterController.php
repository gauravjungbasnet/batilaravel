<?php

namespace App\Http\Controllers\API;

use App\Footer;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FooterController extends Controller
{
    public function store(Request $request){
        $footer = new Footer(
            $request->all()
            );
        $footer->save();
        return response()->json($footer, 201);
    }

    public function index(){
        $header=DB::select('select * from footer');
        return response()->json($header,200);

    }

    public function update(Request $request, $id){

        $phone1 = $request->input('phone1');
        $phone2 = $request->input('phone2');
        $phone3 = $request->input('phone3');
        $email1 = $request->input('email1');
        $email2 = $request->input('email2');
        $email3 = $request->input('email3');
        $facebook = $request->input('facebook');
        $instagram = $request->input('instagram');
        $twitter = $request->input('twitter');
        $open_and_close = $request->input('open_and_close');

        $footer = DB::update('update footer set phone1 = ?,phone2=?,phone3=?,email1=?,email2=?,email3=?,facebook=?,instagram=?,twitter=?,open_and_close=? where id = ?',[$phone1,$phone2,$phone3,$email1,$email2,$email3,$facebook,$instagram,$twitter,$open_and_close,$id]);
        
        return response()->json($footer,200);
    }
    public function destroy(Request $request, Footer $header){
        $header->delete();
        return response()->json(null, 204);
    }
}
