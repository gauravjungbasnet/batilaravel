<?php

namespace App\Http\Controllers\API;

use App\Contact;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function store(Request $request){
        $contact=new Contact([
            'name'=>$request->get('name'),
            'phone'=> $request->get('phone'),
            'email'=> $request->get('email'),
            'subject'=> $request->get('subject'),
            'message'=> $request->get('message')
        ]);
        $contact->save();
        return response()->json($contact, 201);
    }

    public function index(){
        $contact=DB::select('select * from contact');
        return response()->json($contact,200);
    }

    public function update(Request $request, $id){
        // $aboutUs  = update($request->all());
        $name = $request->input('name');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $message = $request->input('message');
        $contact  = DB::update('update contact set name = ?,phone=?,email=?,subject=?,message=? where id = ?',[$name,$phone,$email,$subject,$message,$id]);
        return response()->json($contact,200);
    }
    public function destroy($id){
        $contact = Contact::where('id',$id)->delete();
        return response()->json(null, 204);
    }
}
