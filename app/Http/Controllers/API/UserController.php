<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    // description: login and signup controllers

    public function login(){
        if(Auth::attempt(['email' => request('email'),'password'=>request('password')])){
            $user=Auth::user();
            if(!empty($user->is_deactivated)){
                $message="Sorry Access Denied, Please contact to your administrator";
                return response()->json([$message],401);
            }
            $success['token']=$user->createToken('MyApp')->accessToken;
            return response()->json(['success'=>$success]  , 200);
        }
        else{
            $message="Incorrect email or password";
            return response()->json([$message],401);
        }
    }

 // register
 public function register(Request $request) 
 { 
     $input = $request->all(); 
     $input['password'] = bcrypt($input['password']); 
     $user = User::create($input); 
    //  $success['token'] =  $user->createToken('MyApp')-> accessToken; 
    //  $success['name'] =  $user->name;
     return response()->json(['success'=>$user], 200); 
 }

  // details 
  public function details(Request $request) 
  { 
          $user = auth()->user(); 
          return response()->json($user);
  } 

 
    

}
