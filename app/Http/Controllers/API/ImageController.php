<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use File;

class ImageController extends Controller
{
    public function saveFile(Request $request){
        
  
        $File = $request -> file('myfile');
        $sub_path='files';
        $real_name=$File->getClientOriginalName();
        $unique_name = md5($real_name. time());
        $result = "$unique_name$real_name";
        $destination_path=public_path($sub_path);
        $File->move($destination_path, $result);
        return response()->json($result, 200);
    }

public function deleteFile(Request $request){
    $sub_path='files';
    $result = $request->fileName;
    $destination_path=public_path($sub_path);
    $result = "$destination_path/$result";
    File::delete($result);
    return response()->json('success', 200);
}


public function updateBati(Request $request){
    try{
            if(!empty($request['currentPassword'])){
                $user= User::find($request['id']);
            if(\Hash::check($request['currentPassword'],$user->password)){
                 $validator = \Validator::make($request->all(), [ 
                    'newPassword' => 'required', 
                    'c_newPassword' => 'required|same:newPassword', 
                ]);
            if ($validator->fails()) { 
                $message="New password and confirm password doesnot match";
                return response()->json($message, 401);            
                // return response()->json(['error'=>$validator->errors()], 401);            
            }
                $request['password']= bcrypt($request['newPassword']);
                $user->update($request->all());
                $message = "Password Changed Successfully";
                return response()->json($message,200);
            }else{
                $message = "Current password doesnot matched";
                return response()->json($message,401);
            }
            $user->update($request->all());
            $message = "Password Changed Successfully";
            return response()->json($message,200);
            }else{
            $message = "Please enter current and new password";
            return response()->json($message,400);
            }
            
    }catch(\Exception $exception){
        return response()->json($exception,400);
    }
    
}

}
