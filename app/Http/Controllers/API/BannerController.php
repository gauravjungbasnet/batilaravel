<?php

namespace App\Http\Controllers\API;

use App\Banner;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{

    public function store(Request $request){
        $banner=new Banner([
            'ImageName'=>$request->get('ImageName'),
            'ImagePath'=> $request->get('ImagePath'),
            'Description'=> $request->get('Description'),
            'Order'=> $request->get('Order')
        ]);
        $banner->save();
        return response()->json($banner, 201);
    }

    public function index(){
        $banner=DB::select('select * from banner ORDER BY `Order`  asc ');
        return response()->json($banner,200);

    }

    public function update(Request $request, Banner $banner){
        
        $banner->update($request->all());
        return response()->json($banner,200);
    }
    public function destroy(Request $request, Banner $banner){
        $banner->delete();
        return response()->json(null, 204);
    }
}
