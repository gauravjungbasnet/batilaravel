<?php

namespace App\Http\Controllers\API;

use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function store(Request $request){
        $product=new Product([
            'title'=>$request->get('title'),
            'category_id'=> $request->get('category_id'),
            'material'=> $request->get('material'),
            'core'=> $request->get('core'),
            'thickness'=> $request->get('thickness'),
            'popular_width'=> $request->get('popular_width'),
            'popular_length'=> $request->get('popular_length'),
            'color'=> $request->get('color'),
            'roll_size'=> $request->get('roll_size'),
            'supply_capacity'=> $request->get('supply_capacity'),
        ]);
        $product->save();
        return response()->json($product, 201);
    }

    public function index(){
        $product=DB::select('select * from product');
        return response()->json($product,200);
    }

    public function CategoryWiseProduct($id)
    {
        // $product=DB::select(`select * from product where category_id = $id`);
        $product=DB::table('product')->where('category_id',$id)->get();
        return response()->json($product,200);
    }

    public function update(Request $request, $id){
        // $aboutUs  = update($request->all());
        $title = $request->input('title');
        $category_id = $request->input('category_id');
        $material = $request->input('material');
        $core = $request->input('core');
        $thickness = $request->input('thickness');
        $popular_width = $request->input('popular_width');
        $popular_length = $request->input('popular_length');
        $color = $request->input('color');
        $roll_size = $request->input('roll_size');
        $supply_capacity = $request->input('supply_capacity');
        $imagePath = $request->input('imagePath');
        $product  = DB::update('update product set title = ?,category_id=?,material=?,core=?,thickness=?,popular_width=?,
        popular_length=?,color=?,roll_size=?,supply_capacity=?,imagePath=? where id = ?',
        [$title,$category_id,$material,$core,$thickness,$popular_width,$popular_length,$color,
        $roll_size,$supply_capacity,$imagePath,$id]);
        return response()->json($product,200);
    }
    public function destroy($id){
        $product = Product::where('id',$id)->delete();
        return response()->json(null, 204);
    }
}
