<?php

namespace App\Http\Controllers\API;

use App\OurTeam;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OurTeamController extends Controller
{
    public function store(Request $request){
        $ourteam=new OurTeam([
            'title'=>$request->get('title'),
            'description'=> $request->get('description'),
            'designation'=> $request->get('designation')
        ]);
        $ourteam->save();
        return response()->json($ourteam, 201);
    }

    public function index(){
        $ourteam=DB::select('select * from our_team');
        return response()->json($ourteam,200);
    }

    public function update(Request $request, $id){
        // $aboutUs  = update($request->all());
        $title = $request->input('title');

        $description = $request->input('description');
        $designation = $request->input('designation');
        $imagePath = $request->input('imagePath');
        $ourteam  = DB::update('update our_team set title = ?,description=?,designation=?,imagePath=? where id = ?',[$title,$description,$designation,$imagePath,$id]);
        return response()->json($ourteam,200);
    }
    public function destroy($id){
        $ourteam = OurTeam::where('id',$id)->delete();
        return response()->json(null, 204);
    }
}
