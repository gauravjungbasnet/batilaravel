<?php

namespace App\Http\Controllers\API;

use App\AboutUs;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{
    public function store(Request $request){
        $aboutus=new AboutUs([
            'title'=>$request->get('title'),
            'description'=> $request->get('description'),
            'company_name'=> $request->get('company_name')
        ]);
        $aboutus->save();
        return response()->json($aboutus, 201);
    }

    public function index(){
        $aboutus=DB::select('select * from about_us');
        return response()->json($aboutus,200);
    }

    public function update(Request $request, $id){
        // $aboutUs  = update($request->all());
        $title = $request->input('title');
        $description = $request->input('description');
        $company_name = $request->input('company_name');
        $aboutUs  = DB::update('update about_us set title = ?,description=?,company_name=? where id = ?',[$title,$description,$company_name,$id]);
        return response()->json($aboutUs,200);
    }
    public function destroy(Request $request, AboutUs $aboutus){
        $aboutus->delete();
        return response()->json(null, 204);
    }
}
