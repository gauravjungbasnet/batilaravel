<?php

namespace App\Http\Controllers\API;

use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function store(Request $request){
        $category=new Category([
            'name'=>$request->get('name'),
            'category_code'=> $request->get('category_code'),
        ]);
        $category->save();
        return response()->json($category, 201);
    }

    public function index(){
        $category=DB::select('select * from category');
        return response()->json($category,200);
    }

    public function update(Request $request, $id){
        // $aboutUs  = update($request->all());
        $name = $request->input('name');
        $category_code = $request->input('category_code');
        $imagePath = $request->input('imagePath');
        $category  = DB::update('update category set name = ?,category_code=?,imagePath=? where id = ?',[$name,$category_code,$imagePath,$id]);
        return response()->json($category,200);
    }
    public function destroy($id){
        $category = Category::where('id',$id)->delete();
        return response()->json(null, 204);
    }
}
