<?php

namespace App\Http\Controllers\API;

use App\Testimonials;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestimonialController extends Controller
{
    public function store(Request $request){
        $testimonials=new Testimonials([
            'title'=>$request->get('title'),
            'description'=> $request->get('description'),
            'client_name'=> $request->get('client_name')
        ]);
        $testimonials->save();
        return response()->json($testimonials, 201);
    }

    public function index(){
        $testimonials=DB::select('select * from testimonials');
        return response()->json($testimonials,200);
    }

    public function update(Request $request, $id){
        // $aboutUs  = update($request->all());
        $title = $request->input('title');

        $description = $request->input('description');
        $client_name = $request->input('client_name');
        $imagePath = $request->input('imagePath');
        $testimonials  = DB::update('update testimonials set title = ?,description=?,client_name=?,imagePath=? where id = ?',[$title,$description,$client_name,$imagePath,$id]);
        return response()->json($testimonials,200);
    }
    public function destroy($id){
        $testimonials = Testimonials::where('id',$id)->delete();
        return response()->json(null, 204);
    }
}
