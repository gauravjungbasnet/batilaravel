<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Routes for CMS

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');

Route::resource('banner','API\BannerController');
Route::resource('header','API\HeaderController');
Route::resource('about_us','API\AboutUsController');
Route::resource('our_service','API\OurServiceController');
Route::resource('our_team','API\OurTeamController');
Route::resource('testimonials','API\TestimonialController');
Route::resource('contact','API\ContactController');
Route::resource('category','API\CategoryController');
Route::resource('product','API\ProductController');
Route::resource('footer','API\FooterController');
Route::post('updateBati','API\ImageController@updateBati');


Route::get('getCategoryWiseProduct/{id}','API\ProductController@CategoryWiseProduct');

// Routes for login and sign up

Route::post('login','API\UserController@login');
Route::post('register','API\UserController@register');


Route::post('uploadFile','API\ImageController@saveFile');
Route::post('deleteFile','API\ImageController@deleteFile');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('details',[ 'as' => 'login', 'uses' => 'API\UserController@details'] );
    });

